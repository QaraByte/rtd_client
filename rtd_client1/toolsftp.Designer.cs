﻿namespace rtd_client1
{
    partial class toolsftp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.txtFTP = new System.Windows.Forms.TextBox();
            this.txtLogin = new System.Windows.Forms.TextBox();
            this.txtPassw = new System.Windows.Forms.TextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.lbFTP = new System.Windows.Forms.Label();
            this.lbLogin = new System.Windows.Forms.Label();
            this.lbPassw = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.SuspendLayout();
            // 
            // txtFTP
            // 
            this.txtFTP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFTP.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtFTP.Location = new System.Drawing.Point(75, 26);
            this.txtFTP.Name = "txtFTP";
            this.txtFTP.Size = new System.Drawing.Size(179, 26);
            this.txtFTP.TabIndex = 0;
            // 
            // txtLogin
            // 
            this.txtLogin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtLogin.Location = new System.Drawing.Point(27, 93);
            this.txtLogin.Name = "txtLogin";
            this.txtLogin.Size = new System.Drawing.Size(140, 22);
            this.txtLogin.TabIndex = 1;
            // 
            // txtPassw
            // 
            this.txtPassw.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPassw.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtPassw.Location = new System.Drawing.Point(27, 147);
            this.txtPassw.Name = "txtPassw";
            this.txtPassw.PasswordChar = '*';
            this.txtPassw.Size = new System.Drawing.Size(140, 22);
            this.txtPassw.TabIndex = 2;
            this.txtPassw.MouseMove += new System.Windows.Forms.MouseEventHandler(this.txtPassw_MouseMove);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(27, 175);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(140, 40);
            this.btnSave.TabIndex = 3;
            this.btnSave.Text = "Сохранить";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // lbFTP
            // 
            this.lbFTP.AutoSize = true;
            this.lbFTP.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbFTP.Location = new System.Drawing.Point(23, 29);
            this.lbFTP.Name = "lbFTP";
            this.lbFTP.Size = new System.Drawing.Size(46, 20);
            this.lbFTP.TabIndex = 4;
            this.lbFTP.Text = "ftp://";
            // 
            // lbLogin
            // 
            this.lbLogin.AutoSize = true;
            this.lbLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbLogin.Location = new System.Drawing.Point(24, 74);
            this.lbLogin.Name = "lbLogin";
            this.lbLogin.Size = new System.Drawing.Size(50, 16);
            this.lbLogin.TabIndex = 5;
            this.lbLogin.Text = "Логин:";
            // 
            // lbPassw
            // 
            this.lbPassw.AutoSize = true;
            this.lbPassw.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbPassw.Location = new System.Drawing.Point(24, 128);
            this.lbPassw.Name = "lbPassw";
            this.lbPassw.Size = new System.Drawing.Size(60, 16);
            this.lbPassw.TabIndex = 6;
            this.lbPassw.Text = "Пароль:";
            // 
            // toolsftp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(259, 223);
            this.Controls.Add(this.lbPassw);
            this.Controls.Add(this.lbLogin);
            this.Controls.Add(this.lbFTP);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.txtPassw);
            this.Controls.Add(this.txtLogin);
            this.Controls.Add(this.txtFTP);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "toolsftp";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Настройки FTP";
            this.Load += new System.EventHandler(this.toolsftp_Load);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.toolsftp_MouseMove);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtFTP;
        private System.Windows.Forms.TextBox txtLogin;
        private System.Windows.Forms.TextBox txtPassw;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label lbFTP;
        private System.Windows.Forms.Label lbLogin;
        private System.Windows.Forms.Label lbPassw;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}