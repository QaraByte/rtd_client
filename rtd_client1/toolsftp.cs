﻿using System;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace rtd_client1
{
    public partial class toolsftp : Form
    {
        public toolsftp()
        {
            InitializeComponent();
        }

        string ftp = "";
        string login = "";
        string passw = "";
        string fileName = "ftptools.txt";

        private void btnSave_Click(object sender, EventArgs e)
        {
            //ftp://195.210.46.24/

            if (File.Exists(fileName))                           // Если файл существует
            {
                File.WriteAllText(fileName, "");                 // Специально стираем, все что в файле tools.txt

                NastroikaFTP();
                this.Close();
            }

            if (!File.Exists(fileName))                          // Если файл не существует, то создаем новый
            {
                FileStream fs = File.Create(fileName);
                fs.Close();

                if (txtFTP.Text != "")
                {
                    NastroikaFTP();
                    this.Close();
                }
            }
        }

        private void NastroikaFTP()
        {
            string[] dannie = new string[3];
            dannie[0] = txtFTP.Text;
            dannie[1] = EnCrypt(txtLogin.Text);
            dannie[2] = EnCrypt(txtPassw.Text);

            File.AppendAllLines(fileName, dannie, System.Text.Encoding.GetEncoding(1251));

            MessageBox.Show("Данные сохранены!", "Информация");
        }

        private void toolsftp_Load(object sender, EventArgs e)
        {
            if (File.Exists(fileName))
            {
                string s = File.ReadAllText(fileName, Encoding.GetEncoding(1251));

                string[] sDownload = s.Split(new[] { ' ', '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
                ftp = sDownload[0];
                login = DeCrypt(sDownload[1]);
                passw = DeCrypt(sDownload[2]);

                txtFTP.Text = ftp;
                txtLogin.Text = login;
                txtPassw.Text = passw;
            }
            else
            {
                MessageBox.Show("Файла с настройками не существует!", "Информация");
            }
        }

        string[] qwe = { "q", "w", "e", "r", "t", "y", "u", "i", "o", "p", "a", "s", "d", "f", "g", "h", "j", "k", "l",
                            "z", "x", "c", "v", "b", "n", "m", "Q", "W", "E", "R", "T", "Y", "U", "I", "O", "P",
                            "A", "S", "D", "F", "G", "H", "J", "K", "L", "Z", "X", "C", "V", "B", "N", "M",
                            "1", "2", "3", "4", "5", "6", "7", "8","9", "0",
                            "й","ц","у","к","е","н","г","ш","щ","з","х","ъ","ф","ы","в","а","п","р","о","л","д",
                            "ж", "э", "я", "ч", "с", "м", "и", "т", "ь", "б", "ю"};
        string[] abc = { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r",
                        "s", "t", "u", "v", "w", "x", "y", "z", "A", "B", "C", "D", "E", "F", "G", "H", "I",
                        "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
                        "1","2", "3", "4","5", "6", "7", "8","9", "0",
                        "й","ц","у","к","е","н","г","ш","щ","з","х","ъ","ф","ы","в","а","п","р","о","л","д",
                        "ж","э","я","ч","с","м","и","т","ь", "б","ю"};

        int[] enc;

        private string EnCrypt(string s)                        // функция шифрования
        {
            int n = s.Length;
            enc = new int[n];

            for (int j = 0; j < s.Length; j++)
                for (int i = 0; i < qwe.Length; i++)
                {
                    int a1 = qwe[i].IndexOf(s[j]);
                    if (a1 == 0)
                    {
                        enc[j] = i;
                        i = qwe.Length;         // т.к. буква найдена, можно прервать цикл
                    }
                }

            string encr = "";

            for (int z = 0; z < enc.Length; z++)
            {
                encr += abc[enc[z]];
            }
            return encr;
        }

        int[] denc;

        private string DeCrypt(string s1)                       // функция дешифрования
        {
            int n1 = s1.Length;
            denc = new int[n1];

            for (int f = 0; f < s1.Length; f++)
                for (int g = 0; g < abc.Length; g++)
                {
                    int r = abc[g].IndexOf(s1[f]);
                    if (r == 0)
                    {
                        denc[f] = g;
                        g = abc.Length;
                    }
                }

            string dencr = "";

            for (int a = 0; a < s1.Length; a++)
            {
                dencr += qwe[denc[a]];
            }
            return dencr;
        }

        private void txtPassw_MouseMove(object sender, MouseEventArgs e)
        {
            toolTip1.Show("Только буквы и цифры", txtPassw);
            //toolTip1.ToolTipTitle = "Только буквы и цифры";
        }

        private void toolsftp_MouseMove(object sender, MouseEventArgs e)
        {
            toolTip1.Hide(txtPassw);
        }
    }
}
