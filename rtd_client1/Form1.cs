﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.IO;

namespace rtd_client1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        string ftp = "";
        string login = "";
        string passw = "";

        private void DownLoad(string fileName)
        {
            WebClient wc = new WebClient();
            wc.BaseAddress = "ftp://" + ftp;
            wc.Credentials = new NetworkCredential(login, passw);
            
            bool flag = true;

            try
            {
                do
                {
                    if (!wc.IsBusy)
                    {
                        listBox_Status.Items.Add(DateTime.Now.ToString() + " - " + "Downloading..");
                        toolStripStatusLabel1.Text = DateTime.Now.ToString() + " - " + "Загрузка";
                        wc.DownloadProgressChanged += new DownloadProgressChangedEventHandler(wc_DownloadProgressChanged);
                        wc.DownloadFileCompleted += new System.ComponentModel.AsyncCompletedEventHandler(wc_DownloadFileCompleted);
                        wc.DownloadFileAsync(new Uri(@"ftp://" + ftp + "/files/" + Path.GetFileName(fileName)),
                            //@"g:/" + Path.GetFileName(fileName));
                            @"" + Path.GetFileName(fileName));
                        flag = false;
                    }
                } while (flag);
            }
            catch (Exception ex)
            {
                toolStripStatusLabel1.Text = "Ошибка: " + ex.Message;
            }
        }

        private void wc_DownloadFileCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            listBox_Status.Items.Add(DateTime.Now.ToString() + " - " + "Downloaded");
            if (listBox_Status.Items.Count == 2)
            {
                listBox_Status.Items.Clear();
                listBox_Status.Items.Add("Данные на сервере не найдены.");
                toolStripStatusLabel1.Text = DateTime.Now.ToString() + " - " + "Данных не обнаружено.";
            }
            else
            {
                listBox_Status.Items.Add(DateTime.Now.ToString() + " - " + "Данные загружены");
                toolStripStatusLabel1.Text = DateTime.Now.ToString() + " - " + "Данные загружены";
            }
        }

        private void wc_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            listBox_Status.Items.Add(DateTime.Now.ToString() + " - " + e.ProgressPercentage.ToString() + "% - " + 
                                                e.BytesReceived / 1024 + @"kb/" + e.TotalBytesToReceive / 1024 + "kb");
        }

        private void btnGet_Click(object sender, EventArgs e)
        {
            listBox_Status.Items.Clear();
            btnGet.Enabled = false;                                     // Сделаем кнопку неактивной, чтобы дать 
                                                                        // функции доработать

            string date2 = DateTime.Now.ToShortDateString();            // Определяем дату на сегодня
            string fileName2 = "rtd" + date2 + ".txt";                  // спереди прибавляем rtd, а сзади расширение

            if (File.Exists("ftptools.txt"))                            // Если файл настроек существует
            {
                string s2 = File.ReadAllText("ftptools.txt", Encoding.GetEncoding(1251));   // Считываем файл

                string[] sUpload = s2.Split(new[] { ' ', '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
                ftp = sUpload[0];                                       // IP-адрес
                login = DeCrypt(sUpload[1]);                            // Логин
                passw = DeCrypt(sUpload[2]);                            // Пароль

                DownLoad(fileName2);                                    // Функция загрузки

                timer1.Enabled = true;
                
                btnGet.Enabled = true;                                  // Делаем кнопку обратно активной
            }
            else
            {
                MessageBox.Show("Файла с настройками не существует!", "Информация");
                toolStripStatusLabel1.Text = "Файла с настройками не существует";
            }
        }

        private void StripMenuFTP_Click(object sender, EventArgs e)
        {
            toolsftp tftp = new toolsftp();
            tftp.ShowDialog();
        }

        private void StripMenuExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        string[] qwe = { "q", "w", "e", "r", "t", "y", "u", "i", "o", "p", "a", "s", "d", "f", "g", "h", "j", "k", "l",
                            "z", "x", "c", "v", "b", "n", "m", "Q", "W", "E", "R", "T", "Y", "U", "I", "O", "P",
                            "A", "S", "D", "F", "G", "H", "J", "K", "L", "Z", "X", "C", "V", "B", "N", "M",
                            "1", "2", "3", "4", "5", "6", "7", "8","9", "0",
                            "й","ц","у","к","е","н","г","ш","щ","з","х","ъ","ф","ы","в","а","п","р","о","л","д",
                            "ж", "э", "я", "ч", "с", "м", "и", "т", "ь", "б", "ю"};
        string[] abc = { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r",
                        "s", "t", "u", "v", "w", "x", "y", "z", "A", "B", "C", "D", "E", "F", "G", "H", "I",
                        "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
                        "1","2", "3", "4","5", "6", "7", "8","9", "0",
                        "й","ц","у","к","е","н","г","ш","щ","з","х","ъ","ф","ы","в","а","п","р","о","л","д",
                        "ж","э","я","ч","с","м","и","т","ь", "б","ю"};

        int[] enc;

        private string EnCrypt(string s)                        // функция шифрования
        {
            int n = s.Length;                                   // Длина шифруемого слова
            enc = new int[n];                                   // Массив для найденных индексов

            for (int j = 0; j < s.Length; j++)
                for (int i = 0; i < qwe.Length; i++)
                {
                    int a1 = qwe[i].IndexOf(s[j]);              // Находим индекс буквы в массиве qwe
                    if (a1 == 0)                                // Если равен 0, значит найден
                    {
                        enc[j] = i;                             // Записываем в массив enc найденный индекс
                        i = qwe.Length;                         // т.к. буква найдена, можно прервать цикл
                    }
                }

            string encr = "";

            for (int z = 0; z < enc.Length; z++)
            {
                encr += abc[enc[z]];                            // Зашифрованное слово
            }
            return encr;
        }

        int[] denc;

        private string DeCrypt(string s1)                       // функция дешифрования
        {
            int n1 = s1.Length;
            denc = new int[n1];

            for (int f = 0; f < s1.Length; f++)
                for (int g = 0; g < abc.Length; g++)
                {
                    int r = abc[g].IndexOf(s1[f]);
                    if (r == 0)
                    {
                        denc[f] = g;
                        g = abc.Length;
                    }
                }

            string dencr = "";

            for (int a = 0; a < s1.Length; a++)
            {
                dencr += qwe[denc[a]];
            }
            return dencr;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            string dt = DateTime.Now.ToShortDateString();
            lbDate.Text = "Сегодня: " + dt;

            Zagruzka();
            toolStripStatusLabel1.Text = "Количество человек, записанных на прием: " + listView1.Items.Count.ToString();
        }

        string[] sr_copy;
        List<string> doc = new List<string>();
        string[] s2_copy;

        private void Zagruzka()                                 // Чтение данных из файла и наполнение listView1
        {
            string dt = DateTime.Now.ToShortDateString();
            string filename2 = "rtd" + dt + ".txt";             // Файл с датой на сегодня

            if (File.Exists(filename2))                         // Если найден файл с датой на сегодня, то
            {
                ZagruzkaToLvi(filename2);                       // Загрузка в форму
            }
            else
            {
                var parts0 = dt.Split('.');                     // Разбиваем дату на число, месяц, год
                int d01 = int.Parse(parts0[0]);                 // число
                int d02 = int.Parse(parts0[1]);                 // месяц
                int d03 = int.Parse(parts0[2]);                 // год
                string filename3 = filename2;

                string[] fileTxt = Directory.GetFiles(Environment.CurrentDirectory, "*.txt");       // Все текстовые файлы в каталоге

                if (fileTxt != null)                            // Если хотя бы один файл есть, то продолжаем работать
                    if (fileTxt.Length > 0)
                    {
                        List<int> int_d0 = new List<int>();     // Массив для чисел
                        List<int> int_d1 = new List<int>();     // Массив для месяцев
                        List<int> int_d2 = new List<int>();     // Массив для лет

                        for (int i = 0; i < fileTxt.Length; i++)
                        {
                            string s = Path.GetFileName(fileTxt[i]);        // Получаем из полного пути только имя файла

                            if (s.Contains("rtd"))                          // Проверяем, чтобы в файле был текст rtd
                            {
                                if (s.Length > 14)                          // Длина файла должна быть не менее 14 символов
                                {
                                    string s1 = s.Substring(3, 10);         // Выбираем ту часть, где только дата
                                    var parts = s1.Split('.');              // Разбиваем на число, месяц, год

                                    int d1 = int.Parse(parts[0]);           // число
                                    int d2 = int.Parse(parts[1]);           // месяц
                                    int d3 = int.Parse(parts[2]);           // год

                                    int_d0.Add(d1);                         // собираем все числа
                                    int_d1.Add(d2);                         // собираем все месяца
                                    int_d2.Add(d3);                         // собираем все года
                                }
                                else
                                {
                                    toolStripStatusLabel1.Text = "Данные не обнаружены";
                                    return;
                                }
                            }
                        }

                        int dz = 30;                                        // Переменная для минимального числа
                        string dz1 = "";
                        int dz2 = 30;
                        string dz22 = "";
                        int dz3 = 30;
                        string dz33 = "";

                        var min = int_d0.Where(x => x % 2 == 0).Min();      // Минимальное число в массиве. В работе программы не участвует

                        for (int i1 = 0; i1 < int_d0.Count; i1++)           // Поиск ближайшего числа к сегодняшней дате
                        {
                            int dd1 = d01 - int_d0[i1];                     // Сегодня минус число из массива

                            if (dd1 >= 0)                   // Если больше либо равно нулю. Чтобы исключить отрицательные числа
                                if (dd1 < dz)               // Если меньше, чем было ранее, записываем
                                {
                                    dz = dd1;               // Записываем это минимальное число в переменную dz
                                    dz1 = int_d0[i1].ToString();            // Т.к. минимальное число найдено, то берем из массива int_d0,
                                    if (dz1.Length < 2)                     // то число, которое, мы определяем на данный момент
                                        dz1 = "0" + dz1;                    // Если число состоит из одной цифры,
                                }                                           // Спереди прибавляем ноль
                        }

                        for (int i2 = 0; i2 < int_d1.Count; i2++)           // Определение ближайшего месяца
                        {
                            int dd2 = d02 - int_d1[i2];

                            if (dd2 >= 0)
                                if (dd2 < dz2)
                                {
                                    dz2 = dd2;
                                    dz22 = int_d1[i2].ToString();
                                    if (dz22.Length < 2)
                                        dz22 = "0" + dz22;
                                }
                        }

                        for (int i3 = 0; i3 < int_d2.Count; i3++)           // Определение ближайшего года
                        {
                            int dd3 = d03 - int_d2[i3];

                            if (dd3 >= 0)
                                if (dd3 < dz3)
                                {
                                    dz3 = dd3;
                                    dz33 = int_d2[i3].ToString();
                                }
                        }

                        string ds = "rtd" + dz1 + "." + dz22 + "." + dz33 + ".txt";     // Полное имя файла, чтобы 
                        filename3 = ds;                                                 // программа смогла прочитать его
                    }
                ZagruzkaToLvi(filename3);
            }
        }

        private void ZagruzkaToLvi(string s)                // Загрузка в listView1
        {
            string s2 = File.ReadAllText(s, System.Text.Encoding.GetEncoding(1251));

            string[] s2_down = s2.Split(new[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
            s2_copy = s2_down;

            for (int i = 0; i < s2_down.Length; i++)
            {
                string[] sr = Razbivka(s2_down[i]);         // Разбивка строки на подстроки
                ListViewItem lvi = new ListViewItem(sr);
                listView1.Items.Add(lvi);                   // Добавление элементов в listView1
                sr_copy = sr;                               // Копируем массив в sr_copy
                int n = sr_copy.Length;                     // Длина массива
                doc.Add(sr_copy[7]);
            }
            doc = doc.Distinct().ToList();                  // Убираем повторяющиеся строки из массива
            for (int c = 0; c < doc.Count; c++)
                cbDoc.Items.Add(doc[c]);                    // Добавляем в comboBox
        }

        private string[] Razbivka(string s)                     // Разбивка строки на подстроки
        {
            string[] ss = s.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries);

            string d = ss[2];

            if (IsNumberContains(d))
            {
                string[] ss2 = new string[8];
                List<string> lst = new List<string>();
                for (int i = 0; i < ss.Length; i++)
                {
                    if (i == 2)
                    {
                        lst.Add(" ");
                        lst.Add(ss[i]);
                    }
                    else
                        lst.Add(ss[i]);
                }

                for (int j = 0; j < lst.Count; j++)
                    if (j < 8)
                        ss2[j] = lst[j];
                return ss2;
            }
            return ss;
        }

        private bool IsNumberContains(string input)         // Проверка строки на содержание цифр
        {
            foreach (char c in input)
                if (Char.IsNumber(c))
                    return true;
            return false;
        }

        private void StripMenuAbout_Click(object sender, EventArgs e)
        {
            MessageBox.Show(@"RTD-клиент (англ. Reception to the Doctor) - программа, 
которая отображает пациентов ЦПЗ, записанных на приём в поликлинику.

17.03.2018
Версия 1.1 - Данные загружаются, даже если были получены ранее
           - При выборе врача, внизу выводится количество записей 
Версия 1.0 Автор: Н.Бексатов, январь 2018 pchelpkz16@gmail.com
ГКП на ПХВ ""Центр психического здоровья"" г.Алматы");
        }

        private void cbDoc_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<string> docs = new List<string>();
            listView1.Items.Clear();
            ListViewItem lvi;

            for (int i = 0; i < s2_copy.Length; i++)
            {
                if (s2_copy[i].Contains(cbDoc.Text))
                {
                    string[] sr2 = Razbivka(s2_copy[i]);
                    lvi = new ListViewItem(sr2);
                    listView1.Items.Add(lvi);
                }
            }
            toolStripStatusLabel1.Text = "Количество записей: " + listView1.Items.Count.ToString();

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            string dt = DateTime.Now.ToShortDateString();
            string filename2 = "rtd" + dt + ".txt";
            if (File.Exists(filename2))
            {
                string s2 = File.ReadAllText(filename2, System.Text.Encoding.GetEncoding(1251));

                string[] s2_down = s2.Split(new[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);

                if (listView1.Items.Count == s2_down.Length)
                {
                    toolStripStatusLabel1.Text = "Новых данных нет.";
                    if (s2_down.Length <= 0)
                        File.Delete(filename2);
                    timer1.Enabled = false;
                }
                else
                {
                    int sum = s2_down.Length - listView1.Items.Count;
                    toolStripStatusLabel1.Text = "Добавлено новых записей: " + sum.ToString() +
                                                ". Чтобы увидеть их, перезагрузите программу.";
                    timer1.Enabled = false;
                }
            }
        }
    }
}
